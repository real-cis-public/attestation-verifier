import org.gradle.accessors.dm.LibrariesForLibs

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    idea
    jacoco
}

repositories {
    mavenCentral()
}

val libs = the<LibrariesForLibs>()
val projectJvmTarget = "17"

group = "de.real.cis"
version = "0.1"

dependencies {
    runtimeOnly(libs.kotlin.reflect)
    implementation(libs.kotlin.json)
    implementation(libs.kotlin.logging)
    implementation(libs.kotlin.coroutines)
    implementation(libs.slf4j)
    implementation(libs.logback.classic)
    implementation(libs.logback.core)
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = projectJvmTarget
        }
    }
}