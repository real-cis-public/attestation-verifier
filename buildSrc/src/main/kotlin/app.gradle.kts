import org.gradle.accessors.dm.LibrariesForLibs

val libs = the<LibrariesForLibs>()

plugins {
    id("essentials")
    id("org.springframework.boot")
}

tasks.withType(AbstractArchiveTask::class) {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
    this.archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
}