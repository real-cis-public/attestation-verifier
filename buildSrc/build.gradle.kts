plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

val libs = the<org.gradle.accessors.dm.LibrariesForLibs>()

dependencies {
    implementation(libs.kotlin.jvm)
    implementation(libs.kotlin.spring)
    implementation(libs.kotlin.serialization)
    implementation(libs.spring.boot)
    implementation(files(libs.javaClass.superclass.protectionDomain.codeSource.location))
}