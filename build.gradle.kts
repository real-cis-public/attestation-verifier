plugins { id("app") }

dependencies {
    implementation(libs.conclave.mail)
    implementation(libs.conclave.common)
    implementation(libs.conclave.host)
    implementation(libs.bouncy.castle.provider)
    implementation(libs.bouncy.castle.bcpkix)
}