package de.real.cis.attestation.util

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import java.nio.file.Files
import java.nio.file.Paths

private val logger = KotlinLogging.logger { }

class AttestationVerifier {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            loadBCProvider()
            if (args.size != 1 && args.size != 2) {
                println("Usage: java -jar attestation-util.jar <attestation> <confidential-value-to-encrypt[optional]>")
                return
            }
            logger.info("Verifying deployment attestation report $args")
            val attestation = EnclaveDeploymentInfo.deserialize(Files.readString(Paths.get(args[0])))
            logger.info("Generated Attestation Report")
            logger.info("\n$attestation")
            if (args.size == 2) {
                val deploymentReport = Json.decodeFromString<DeploymentReport>(String(attestation.deploymentReport))
                logger.info("\nEncrypted result of ${args[1]} encrypted with ECIES with ${deploymentReport.identityPublicKeyB64}")
                logger.info("\n${encryptWitECIES(args[1], deploymentReport.identityPublicKeyB64)}")
            }
        }
    }
}