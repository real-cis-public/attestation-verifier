package de.real.cis.attestation.util

import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.nio.Buffer
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import java.security.KeyFactory
import java.security.PublicKey
import java.security.Security
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher

fun loadBCProvider() {
    Security.addProvider(BouncyCastleProvider())
}

fun fromB64(input: String): ByteArray = Base64.getDecoder().decode(input)
fun toB64(bytes: ByteArray): String = Base64.getEncoder().encodeToString(bytes)

private fun decodeECPublicKey(base64Key: String): PublicKey {
    val keyFactory: KeyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME)
    val publicBytes: ByteArray = org.bouncycastle.util.encoders.Base64.decode(base64Key)
    val keySpec = X509EncodedKeySpec(publicBytes)
    return keyFactory.generatePublic(keySpec)
}

fun encryptWitECIES(plainText: String, publicKeyB64: String): String {
    val cipher = Cipher.getInstance("ECIES")
    cipher.init(Cipher.ENCRYPT_MODE, decodeECPublicKey(publicKeyB64))
    return toB64(cipher.doFinal(plainText.toByteArray()))
}

fun ByteBuffer.getBytes(length: Int): ByteArray = ByteArray(length).also { get(it) }

fun ByteBuffer.getIntLengthPrefixString(): String = getIntLengthPrefixSlice().getRemainingString()

fun ByteBuffer.getRemainingString(): String = Charsets.UTF_8.decode(this).toString()

fun ByteBuffer.getIntLengthPrefixBytes(): ByteArray = getBytes(int)

fun ByteBuffer.addPosition(delta: Int): ByteBuffer {
    (this as Buffer).position(position() + delta)
    return this
}

fun ByteBuffer.getSlice(size: Int): ByteBuffer {
    if (size > remaining()) throw BufferUnderflowException()
    val slice = slice()
    (slice as Buffer).limit(size)
    addPosition(size)
    return slice
}

fun ByteBuffer.getIntLengthPrefixSlice(): ByteBuffer = getSlice(int)