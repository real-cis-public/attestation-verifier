package de.real.cis.attestation.util

import kotlinx.serialization.Serializable

@Serializable
data class DeploymentReport(
    val deploymentQualifier: String,
    val nextCloudPublicKeyB64: String,
    val rootCaPublicKeyB64: String,
    val redisPublicKeyB64: String,
    val mariadbPublicKeyB64: String,
    val fastcgiPublicKeyB64: String,
    val identityPublicKeyB64: String,
    val kdsReportHash: String,
) {
    override fun toString() = """
        Deployment Report Summary:
          - Deployment Qualifier: $deploymentQualifier
          - NextCloudPublicKeyB64: $nextCloudPublicKeyB64
          - Root CA public key for component encrypted communications: $rootCaPublicKeyB64
          - RedisPublicKeyB64: $redisPublicKeyB64
          - MariaDBPublicKeyB64: $mariadbPublicKeyB64
          - FastCgiPublicKeyB64: $fastcgiPublicKeyB64
          - IdentityPublicKeyB64: $identityPublicKeyB64
          - KDS ReportHash: $kdsReportHash      
    """.trimIndent()
}