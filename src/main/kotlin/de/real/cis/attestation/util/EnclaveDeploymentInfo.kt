@file:Suppress("ArrayInDataClass")

package de.real.cis.attestation.util

import com.r3.conclave.common.*
import com.r3.conclave.common.internal.Cursor
import com.r3.conclave.common.internal.SgxReportBody
import com.r3.conclave.common.internal.SgxSignedQuote
import com.r3.conclave.common.internal.attestation.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.nio.ByteBuffer

@Serializable
data class SerializedEnclaveDeploymentInfo(val attestationB64: String, val deploymentReport: String)

class EnclaveDeploymentInfoImpl(override val deploymentReport: ByteArray, private val attestation: Attestation) : EnclaveDeploymentInfo {
    override val enclaveInfo: EnclaveInfo
    override val securityInfo: EnclaveSecurityInfo

    init {
        val reportBody = attestation.reportBody
        val expectedReportDataHash = SHA512Hash.hash(deploymentReport)
        val reportDataHash = SHA512Hash.get(reportBody[SgxReportBody.reportData].read())
        require(expectedReportDataHash == reportDataHash) {
            "The report data provided by the enclave ($reportDataHash) does not match the data in the serialized EnclaveDeploymentInfo ($expectedReportDataHash)."
        }
        enclaveInfo = EnclaveInfo(
            SHA256Hash.get(reportBody[SgxReportBody.mrenclave].read()),
            SHA256Hash.get(reportBody[SgxReportBody.mrsigner].read()),
            reportBody[SgxReportBody.isvProdId].read(),
            reportBody[SgxReportBody.isvSvn].read() - 1,
            attestation.enclaveMode
        )
        securityInfo = SGXEnclaveSecurityInfo(
            attestation.securitySummary, attestation.securityReason, attestation.timestamp, OpaqueBytes(reportBody[SgxReportBody.cpuSvn].bytes)
        )
    }

    override fun toString() = """
        Remote attestation for enclave ${enclaveInfo.codeHash}:
          - Mode: ${enclaveInfo.enclaveMode}
          - Code signer: ${enclaveInfo.codeSigningKeyHash}
          - Deployment Report: ${String(deploymentReport)}
          - Product ID: ${enclaveInfo.productID}
          - Revocation level: ${enclaveInfo.revocationLevel}
        
        Assessed security level at ${securityInfo.timestamp} is ${securityInfo.summary}
          - ${securityInfo.reason}
    """.trimIndent()

    override fun serialize(): String {
        val bAos = ByteArrayOutputStream()
        val dos = DataOutputStream(bAos)
        attestation.writeTo(dos)
        return Json.encodeToString(SerializedEnclaveDeploymentInfo(toB64(bAos.toByteArray()), String(deploymentReport)))
    }
}

interface EnclaveDeploymentInfo {

    val enclaveInfo: EnclaveInfo
    val securityInfo: EnclaveSecurityInfo
    val deploymentReport: ByteArray
    fun serialize(): String

    companion object {
        @JvmStatic
        fun deserialize(serialized: String): EnclaveDeploymentInfo {
            try {
                val holder = Json.decodeFromString<SerializedEnclaveDeploymentInfo>(serialized)
                return EnclaveDeploymentInfoImpl(holder.deploymentReport.toByteArray(Charsets.UTF_8), getFromBuffer(ByteBuffer.wrap(fromB64(holder.attestationB64))))
            } catch (e: Exception) {
                throw RuntimeException("Failed to deserialize EnclaveDeploymentInfo", e)
            }
        }

        private fun getFromBuffer(buffer: ByteBuffer): Attestation {
            buffer.get()
            val attestationSlice = buffer.getIntLengthPrefixSlice()
            val size = SgxSignedQuote.INSTANCE.size(attestationSlice)
            val signedQuote = Cursor.wrap(SgxSignedQuote.INSTANCE, attestationSlice.getBytes(size)).asReadOnly()
            val collateral = collateral(attestationSlice)
            return DcapAttestation(signedQuote, collateral)
        }

        private fun collateral(buffer: ByteBuffer): QuoteCollateral {
            val version = buffer.getIntLengthPrefixString().toInt()
            val rawPckCrlIssuerChain = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawRootCaCrl = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawPckCrl = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawTcbInfoIssuerChain = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawSignedTcbInfo = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawQeIdentityIssuerChain = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            val rawSignedQeIdentity = OpaqueBytes(buffer.getIntLengthPrefixBytes())
            return QuoteCollateral(
                version, rawPckCrlIssuerChain, rawRootCaCrl, rawPckCrl, rawTcbInfoIssuerChain, rawSignedTcbInfo, rawQeIdentityIssuerChain, rawSignedQeIdentity
            )
        }
    }
}